﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieFranchises.Migrations
{
    public partial class ManyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharactersId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharactersId, x.MoviesId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "The Monk", "Male", "Sognir", "" },
                    { 2, "The Druid", "Female", "Aunrae-Oyi", "" },
                    { 3, "The Gunslinger", "Male", "Akim", "" },
                    { 4, "The Ranger", "Female", "Shaxana", "" },
                    { 5, "The Librarian", "Male", "Zahariel", "" },
                    { 6, "The Vanguard", "Male", "Flaen", "" }
                });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Director", "Title" },
                values: new object[] { "Daniel", "The Citadel" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Director", "FranchiseId", "ReleaseYear", "Title" },
                values: new object[] { "Daniel", 3, 2022, "The Temple of Time" });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Director", "Title" },
                values: new object[] { "Rik", "The Lord of Skulls" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Director", "FranchiseId", "ReleaseYear", "Title" },
                values: new object[] { "Rik", 1, 2020, "Assassinations" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Poster", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 7, "Tim", 2, "War / Horror", "", 2021, "The Space Hulk", "" },
                    { 8, "Tim", 2, "War", "", 2022, "Steel Legion", "" },
                    { 9, "Tim", 2, "War", "", 2022, "A brother returns", "" },
                    { 10, "Daniel", 3, "Adventure / Horror", "", 2018, "The Citadel", "" },
                    { 11, "Daniel", 3, "Adventure", "", 2018, "The Captain", "" },
                    { 12, "Daniel", 3, "Adventure", "", 2019, "The Mountain Erupts", "" },
                    { 13, "Daniel", 1, "Adventure", "", 2020, "The God Beasts", "" },
                    { 14, "Daniel", 1, "Adventure / Puzzles", "", 2022, "The Temple of Time", "" },
                    { 15, "Daniel", 1, "Adventure / Puzzles", "", 2022, "The Ruins", "" }
                });
        }
    }
}
